FROM python:3.9-slim

RUN apt-get update &&\
    apt-get install -y git build-essential libpq-dev &&\
    rm -rf /var/lib/apt/lists/*

RUN useradd dummy

WORKDIR /opt
COPY requirements.txt /opt
RUN pip3 install gunicorn
RUN pip3 install -r requirements.txt
COPY run.py /opt
ADD application /opt/application

USER dummy


if __name__ == "__main__":
    from dotenv import load_dotenv
    load_dotenv()
    from application import Application
    app = Application()
    app.run(port=6502)

else:
    from application import Application
    app = Application()

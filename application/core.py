import json
import zipfile

from flask import Flask, request, abort, send_file
from flask_restful import Api, Resource
import pandas as pd

from .http import register_error_handlers, success
from .models import db, Meta
from .utils import get_meta, get_domains, get_models,\
    get_domain_ids, get_new_id, get_domain_name_ids, init_db,\
    import_from_dump, zip_dump, dump_all_domains


class Application(Flask):

    def __init__(self):
        super().__init__(__package__)
        register_error_handlers(self)

        # check if database needs to be initialized:
        db.connect()
        tables = db.get_tables()
        if 'meta' not in tables:
            init_db()

        self.api = Api(self)
        self.api.add_resource(Domains, '/')
        self.api.add_resource(
            DomainModels, '/<domain>')
        self.api.add_resource(
            DomainModeldatasets, '/<domain>/<model>')

        self.api.add_resource(
            Exporter,
            '/_export/<domain>/<model>', '/_export/<domain>', '/_export')

        self.api.add_resource(
            Importer, '/_import')


class Exporter(Resource):

    def get(self, domain=None, model=None):
        """A a zip file containing the dump of database content."""
        dump = {}

        if domain == "_meta":
            query = Meta.select()
            dump = [x for x in query.dicts()]
            dump = {"_meta": dump}

        elif domain is None:
            dump = dump_all_domains()

        file = zip_dump(dump)
        return send_file(
            file, attachment_filename='dump.zip', as_attachment=True)


class Importer(Resource):

    def post(self):
        f = request.files['dump']
        number_of_entries = 0
        with zipfile.ZipFile(f) as zf:
            with zf.open('dump.json') as content:
                dump = json.loads(content.read())
                number_of_entries = import_from_dump(dump)
        return success(200, 'imported', count=number_of_entries)


class Domains(Resource):

    def get(self):
        return get_domains()


class DomainModels(Resource):

    def get(self, domain):
        if domain == '_meta':
            from playhouse.shortcuts import model_to_dict
            return [model_to_dict(x) for x in Meta.select()]
        else:
            return get_models(domain)

    def post(self, domain):
        if domain != "_meta":
            abort(405)

        datasets = request.get_json(silent=True)
        if not datasets:
            abort(400, 'The request did not include valid JSON data.')
        if not isinstance(datasets, list):
            datasets = [datasets]

        domain_ids = get_domain_ids()
        domain_name_ids = {}
        for domain in domain_ids.keys():
            domain_name_ids[domain] = get_domain_name_ids(domain)

        for dataset in datasets:
            if 'domain' not in dataset or 'name' not in dataset or\
                    'dtype' not in dataset:
                abort(400, 'The request contains incomplete JSON data.')

            # if domain id does not exist, assign a new one
            domain_id = domain_ids.get(dataset['domain'])
            if domain_id is None:
                domain_id = get_new_id(domain_ids)
                domain_ids[dataset['domain']] = domain_id
                domain_name_ids[dataset['domain']] = {}
            dataset['domain_id'] = domain_id

            # if name id does not exist, assign a new one
            name_id = domain_name_ids[dataset['domain']].get(dataset['name'])
            if name_id is None:
                name_id = get_new_id(domain_name_ids[dataset['domain']])
                domain_name_ids[dataset['domain']][dataset['name']] = name_id
            dataset['name_id'] = name_id

        with db.atomic():
            for dataset in datasets:
                Meta.insert(
                    domain=dataset['domain'],
                    domain_id=dataset['domain_id'],
                    name=dataset['name'],
                    name_id=dataset['name_id'],
                    description=dataset.get('description'),
                    unit=dataset.get('unit'),
                    dtype=dataset['dtype'],
                    ).on_conflict_ignore().execute()

        if len(datasets) == 1:
            return success(201, 'created', count=1)
        else:
            return success(201, 'created', count=len(datasets))

    def delete(self, domain):
        raise NotImplementedError


class DomainModeldatasets(Resource):

    def get(self, domain, model):

        if model == '_meta':
            from playhouse.shortcuts import model_to_dict
            return [
                model_to_dict(x) for x in Meta.select().where(
                    Meta.domain == domain)]

        result = get_meta(domain, model)
        if result is None:
            abort(404)

        table, domain_id, name_id = result

        query = table.select().where(
            table.domain_id == domain_id,
            table.name_id == name_id)

        # filtering
        for key in request.args.keys():
            if not key.startswith('_'):
                for values in request.args.getlist(key):

                    if key == 'time':

                        if ':' not in values:
                            values = values.split(',')
                            values = [
                                pd.Timestamp(v).to_pydatetime()
                                for v in values]
                            query = query.where(table.time << values)

                        elif values.startswith('in:'):
                            values = values[3:].split(',')
                            values = [
                                pd.Timestamp(v).to_pydatetime()
                                for v in values]
                            query = query.where(table.time << values)

                        elif values.startswith('lt:'):
                            values = values[3:].split(',')
                            value = [pd.to_datetime(v) for v in values][0]
                            query = query.where(table.time < value)

                        elif values.startswith('le:'):
                            values = values[3:].split(',')
                            value = [pd.to_datetime(v) for v in values][0]
                            query = query.where(table.time <= value)

                        elif values.startswith('gt:'):
                            values = values[3:].split(',')
                            value = [pd.to_datetime(v) for v in values][0]
                            query = query.where(table.time > value)

                        elif values.startswith('ge:'):
                            values = values[3:].split(',')
                            value = [pd.to_datetime(v) for v in values][0]
                            query = query.where(table.time >= value)

                        elif values.startswith('nin:'):
                            values = values[4:].split(',')
                            value = [pd.to_datetime(v) for v in values][0]
                            query = query.where(~(table.time << values))

                        else:
                            raise NotImplementedError

                    elif key == 'value':

                        if ':' not in values:
                            values = values.split(',')
                            query = query.where(table.value << values)

                        elif values.startswith('in:'):
                            values = values[3:].split(',')
                            query = query.where(table.value << values)

                        elif values.startswith('lt:'):
                            values = values[3:].split(',')
                            value = values[0]
                            query = query.where(table.value < value)

                        elif values.startswith('le:'):
                            values = values[3:].split(',')
                            value = values[0]
                            query = query.where(table.value <= value)

                        elif values.startswith('gt:'):
                            values = values[3:].split(',')
                            value = values[0]
                            query = query.where(table.value > value)

                        elif values.startswith('ge:'):
                            values = values[3:].split(',')
                            value = values[0]
                            query = query.where(table.value >= value)

                        elif values.startswith('nin:'):
                            values = values[4:].split(',')
                            value = values[0]
                            query = query.where(~(table.value << values))

                        else:
                            raise NotImplementedError

        # sorting
        for value in request.args.getlist('_sort'):
            values = value.split(',')
            for value in values:
                if value.startswith('-'):
                    query = query.order_by(getattr(table, value[1:]).desc())
                elif value.startswith('+'):
                    query = query.order_by(getattr(table, value[1:]))
                else:
                    query = query.order_by(getattr(table, value))

        # limiting
        if '_limit' in request.args.keys():
            limit = int(request.args.get('_limit'))
            query = query.limit(limit)

        query = query.select(
            # pe.fn.TO_CHAR(table.time, 'YYYY-MM-DD"T"HH24:MI:SS.US'),  # slow
            table.time.cast('VARCHAR'),
            table.value)

        data = [x for x in query.dicts()]
        return data

    def post(self, domain, model):
        datasets = request.get_json(silent=True)

        if not datasets:
            abort(400, 'The request did not include valid JSON data.')
        if not isinstance(datasets, list):
            datasets = [datasets]

        if model == '_meta':
            for x in datasets:
                x['domain'] = domain
            return DomainModels().post('_meta')

        result = get_meta(domain, model)
        if result is None:
            abort(404)
        else:
            table, domain_id, name_id = result

        data = []
        for dataset in datasets:
            data.append({
                "domain_id": domain_id,
                "name_id": name_id,
                "time": pd.to_datetime(dataset["time"]),
                "value": dataset["value"],
            })

        with db.atomic():
            table.insert_many(data, fields=[
                    table.domain_id,
                    table.name_id,
                    table.time,
                    table.value,
                ]).on_conflict(
                conflict_target=[table.time],
                preserve=[table.time],
            ).execute()

        if len(datasets) == 1:
            return success(201, 'created', count=1)
        else:
            return success(201, 'created', count=len(datasets))

    def put(self, domain, model):
        raise NotImplementedError

    def patch(self, domain, model):
        raise NotImplementedError

    def delete(self, domain, model):
        raise NotImplementedError
